﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_X___Escaner
{
	public partial class Form1 : Form
	{
		string Ruta = "D:\\Documentos\\Pdf y Word\\UTH\\Ingeniería\\arquitectura de software\\Proyectos C#\\Proyecto X - Escaner\\Resources";
		string Codigo = "";
		string[,] Productos =
		{
		
			{"1",		"goku",          "500.00 $",		"Imagen1.png"},
			{"2",		"spider-man",         "650.00 $",      "Imagen2.png"},
			{"3",		"iron-man",       "750.00 $",      "Imagen3.png"},
			{"4",		"batman",    "600.00 $",       "Imagen4.png"},
			{"5",		"denji",        "900.00 $",        "Imagen5.png"}
		};

		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			label1.Location = new Point(this.Width / 2 - label1.Width / 2, 100);

			pictureBox1.Location = new Point(this.Width / 2 - pictureBox1.Width / 2, this.Height / 2 - pictureBox1.Height / 2);

			label2.Location = new Point(this.Width / 2 - label2.Width / 2, this.Height - label2.Height );
		}

		private void Form1_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar != 13)
			{
				Codigo += e.KeyChar;
			}
			else
			{
				MessageBox.Show("Codigo " + Codigo);
				for (int i = 0; i  < 5; i++)
				{
					if (Codigo == Productos[i,0])
					{
						label3.Text = "Nombre " + Productos[i, 1];
						label3.Visible = true;

						label4.Text = "Precio " + Productos[i, 2];
						label4.Visible = true;

						pictureBox1.Image = Image.FromFile(Ruta + @"\" + Productos[i, 3]);

					}
				}
				Codigo = "";
			}
		}

		private void timer1_Tick(object sender, EventArgs e)
		{
			//pictureBox1.Image = Image.FromFile(Ruta + @"\Lector.gif");
			//label2.Text = "Pase el Producto por el escaner";
			//timer1.Enabled = false;
			//label4.Visible = false;
			//label3.Visible = false;
		}

		private void label1_Click(object sender, EventArgs e)
		{

		}
	}
}
